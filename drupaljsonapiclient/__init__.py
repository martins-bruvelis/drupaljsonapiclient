import pkg_resources

from .session import Session
from .filter import Filter, Inclusion, Modifier
from .common import ResourceTuple

__version__ = '1.1.1'
